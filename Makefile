CC=gcc
CFLAGS=-pipe -Wall -O3 -static -static-libgcc

all: web

clean:
	rm *.o *.a

htmlfunc.o: htmlfunc.c htmlfunc.h
	$(CC) $(CFLAGS) -c htmlfunc.c -o htmlfunc.o

sqlfunc.o: sqlfunc.c sqlfunc.h
	$(CC) $(CFLAGS) -c sqlfunc.c -o sqlfunc.o

libglenncgi.a: htmlfunc.o sqlfunc.o
	ar -cvq libglenncgi.a htmlfunc.o sqlfunc.o

main.o: main.c 
	$(CC) $(CFLAGS) -c main.c -o main.o

glenncgi: main.o libglenncgi.a
	$(CC) $(CFLAGS) main.o libglenncgi.a -lsqlite3 -o glenncgi
	strip glenncgi

updatedb:
	cp glenncgi.db webroot/glenncgi.db

web: glenncgi updatedb clean
	cp glenncgi webroot/index.dank


