#include <sqlite3.h>
#include <stdio.h>


#include "sqlfunc.h"
#include "config.h"

int * getposts( size_t size) {

	// need one extra int for the count
    int *r = malloc( size + sizeof(int) );


    sqlite3 *db;
    char *err_msg = 0;
    sqlite3_stmt *res;
    
    int rc = sqlite3_open(database, &db);
    
    if (rc != SQLITE_OK) {
        
        fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
    }
    
    char *sql = "SELECT Id FROM Posts";
        
    rc = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    
    if (rc != SQLITE_OK) {
        fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    }
    
    int step = sqlite3_step(res);
    int counter = 1 ; 
    while (step == SQLITE_ROW)
    {
	    r[counter] = sqlite3_column_int(res, 0);
	    step = sqlite3_step(res);
	    counter++;
	    r[0] = counter ; // so we can tell the total later on.
    }

    sqlite3_finalize(res);
    sqlite3_close(db);
 

    return  r;


}




int printpost(int postindex) {
    
    sqlite3 *db;
    char *err_msg = 0;
    sqlite3_stmt *res;
    
    int rc = sqlite3_open(database, &db);
    
    if (rc != SQLITE_OK) {
        
        fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        
        return 1;
    }
    
    char *sql = "SELECT Id, Name, Data FROM Posts WHERE Id = ?";
        
    rc = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    
    if (rc == SQLITE_OK) {
        
        sqlite3_bind_int(res, 1, postindex);
    } else {
        
        fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
    }
    
    int step = sqlite3_step(res);
    
    if (step == SQLITE_ROW) {
        
        //printf("Post index %s: ", sqlite3_column_text(res, 0));
        //printf("%s\n", sqlite3_column_text(res, 1));
        //printf("%s\n", sqlite3_column_text(res, 2));
	printf("<div class=apost><div class=posttitle>%s</div><div class=postbody>%s</div></div>\n", sqlite3_column_text(res, 1), sqlite3_column_text(res, 2));
        
    } 

    sqlite3_finalize(res);
    sqlite3_close(db);
    
    return 0;
}
